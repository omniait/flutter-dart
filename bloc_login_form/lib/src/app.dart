// Basic material components
import 'package:flutter/material.dart';

// App components
// Pages/Screens
import './screens/login_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Hello Bloc - Log me in",
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}