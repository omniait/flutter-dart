// Some basic utility widgets/functions
import 'package:flutter/material.dart';

Widget addMargin({double value: 20}) {
  return Container(
    margin: EdgeInsets.all(value),
  );
}