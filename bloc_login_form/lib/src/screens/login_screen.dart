// Basic material widgets
import 'package:flutter/material.dart';

// Utilities
import '../utils/utils.dart';

class LoginScreen extends StatelessWidget {
  // Input fields
  // Email field
  TextField emailField() {
    return TextField(
      // Change keyboard type to email to help user
      keyboardType: TextInputType.emailAddress,
      // Some standard input decoration, such as the hint and the label
      decoration: InputDecoration(
        hintText: "you@example.com",
        labelText: "Email"
      ),
      onChanged: (newValue) {
        print(newValue);
      },
    );
  }
  // Password field
  TextField passwordField() {
    return TextField(
      // Hide text
      obscureText: true,
      // Some standard input decoration, such as the hint and the label
      decoration: InputDecoration(
        hintText: "very_secret_very_h4rd2h4ck",
        labelText: "Password"
      ),
      onChanged: (newValue) {
        print(newValue);
      },
    );
  }
  // Submit button
  RaisedButton submitButton() {
    return RaisedButton(
      child: Text("Login"),
      onPressed: () {},
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(14),
      child: Column(
        children: <Widget>[
          emailField(),
          passwordField(),
          addMargin(),
          submitButton(),
        ],
      ),
    );
  }
}