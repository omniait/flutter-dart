// Basic material widgets
import 'package:flutter/material.dart';

// Main app component
import './src/app.dart';

// Entrypoint
void main() {
  runApp(App());
}